(defproject de.uni-stuttgart.iaas.ipsm/protocols "0.0.1-SNAPSHOT"
  :description "Interfaces, protocols, and XML schema of the whole project."
  :license {:name "Eclipse Public License - v 1.0"
            :url "http://www.eclipse.org/legal/epl-v10.html"
            :comments "same as Clojure"}
  :min-lein-version "2.5.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [com.taoensso/timbre "4.1.4"]
                 [org.clojure/data.xml "0.0.8"]
                 [uni-stuttgart.ipsm/jaxb-edn-conversion "0.0.1-SNAPSHOT"]
                 [org.apache.cxf/cxf-rt-frontend-jaxws "3.1.0"]]
  :plugins [[clj-jaxb/lein-xjc "0.2.0-SNAPSHOT"]]
  :xjc-plugin {:xjc-calls [{:xsd-file "src/main/resources/IPSM-v0.1.xsd"}]}
  :uberjar-name "protocols.jar"
  :source-paths ["src/main/clj"]
  :resource-paths ["src/main/resources"]
  :java-source-paths ["src/main/java"])
