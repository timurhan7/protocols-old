package uni_stuttgart.ipsm.protocols.integration;

import java.io.InputStream;


/**
 * Represents a deployable object
 * @author sungurtn
 *
 */
public interface Deployable {
    /**
     * Get target runtime id of the deployable
     * @return
     */
    String getTargetRuntime();

    /**
     * Returns deployable represented by this deployable
     * @return
     */
    InputStream getDeployable();
}
