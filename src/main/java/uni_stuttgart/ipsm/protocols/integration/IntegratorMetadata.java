package uni_stuttgart.ipsm.protocols.integration;

import java.net.URI;


/**
 * Contains meta-data about a provider.
 * @author sungurtn
 *
 */
public interface IntegratorMetadata {
    /**
     * Get the name of the resource provider
     * @return
     */
    String getName();

    /**
     * Return the id of the resource provider
     * It's a URI
     * @return
     */
    String getTargetNamespace();

    /**
     * Returns the address of the respective URI
     * returned String should be convertable to a URI
     * Uses for REST based connection with this integrator.
     * @return
     */
    URI getUri();


}
