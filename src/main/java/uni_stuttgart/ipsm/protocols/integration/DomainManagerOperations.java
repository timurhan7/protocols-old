package uni_stuttgart.ipsm.protocols.integration;

import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.Definitions;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;

/**
 * This class needs to be implemented as a provider implementation.
 *
 * @author sungurtn
 *
 */
public abstract interface DomainManagerOperations {
    /**
     * TOSCA Definitions file that contains different resources and relationships
     * the list of individuals of resources.
     * @return
     */
    Definitions listDomain();


    /**
     * Resource type and information about the intention. Result returns a DeployableArtifact
     * @param
     * intentionInformation defines the intention that this resource relates to
     * type of the desired resource or relationship
     * @return
     */
    Deployable getDeployable(QName type, TIntention intentionInformation);



    /**
     * Return target execution environment of the resource engager or for the resource type
     * @param
     * type of the resource
     * @return
     */
    String getTargetExecutionEnvironment(String resourceType);

    /**
     * To get the actual imported data
     * @return
     */

    List<Import> getImports();

}
