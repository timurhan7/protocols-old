package uni_stuttgart.ipsm.protocols.integration.operations;


/**
 * Operation callback is passed to each operation and called at the end of each operation
 * @author sungurtn
 *
 */
public interface OperationCallback {

    /**
     * 
     * @param outputValues
     */
    void onSuccess(Object outputParameters);
    

    /**
     *
     * @param errorDescription
     */
    void onError(Object outputParameters);



}
