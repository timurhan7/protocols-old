package uni_stuttgart.ipsm.protocols.integration.operations.lifecycle;

import java.io.InputStream;


import uni_stuttgart.ipsm.protocols.integration.operations.OperationRealization;


public abstract class BaseLifecycleOperation implements OperationRealization {
	
	public static String LIFECYCLE_OPERATIONS_TARGET_NAMESPACE = "http://www.uni-stuttgart.de/iaas/ipsm/operations/life-cycle/";
	public static String TARGET_NAMESPACE = "http://www.uni-stuttgart.de/iaas/ipsm/v0.1/";
	public static String XSD_FILE_NAME = "IPSM-v0.1.xsd";
	
	@Override
	public InputStream getTypeDefinition() {
		return getClass().getResourceAsStream(XSD_FILE_NAME);
	}

	@Override
	public String getTypeDefinitionsTargetNamespace() {
		return TARGET_NAMESPACE;
	}

}
