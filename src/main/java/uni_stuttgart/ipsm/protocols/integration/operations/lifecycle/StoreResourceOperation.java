package uni_stuttgart.ipsm.protocols.integration.operations.lifecycle;



import org.oasis_open.docs.tosca.ns._2011._12.TOperation;



public abstract class StoreResourceOperation extends BaseLifecycleOperation {

	public static String OPERATION_NAME = LIFECYCLE_OPERATIONS_TARGET_NAMESPACE + "store-resource";

	
	@Override
	public TOperation getOperationDefinition() {
		// define acquirement operation
		// TODO complete the list of parameters
		TOperation operation = new TOperation();
		operation.setName(OPERATION_NAME);
		return operation;
	}


}
