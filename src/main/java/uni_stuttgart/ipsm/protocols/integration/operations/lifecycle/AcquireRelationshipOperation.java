package uni_stuttgart.ipsm.protocols.integration.operations.lifecycle;



import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TOperation;
import org.oasis_open.docs.tosca.ns._2011._12.TParameter;


public abstract class AcquireRelationshipOperation extends BaseLifecycleOperation {

	public static String OPERATION_NAME = LIFECYCLE_OPERATIONS_TARGET_NAMESPACE + "acquire-relationship";
	// name of the type must match the XSD property specification
	public static String TARGET_RESOURCE_PARAMETER_NAME = "TargetResource";
	// name of the type must match the XSD property specification
	public static String SOURCE_RESOURCE_PARAMETER_NAME = "SourceResource";	


	
	@Override
	public TOperation getOperationDefinition() {
		// define acquirement operation
		TOperation operation = new TOperation();
		operation.setName(OPERATION_NAME);
		//Input perameters
		// Preconditioner relationships
		// All relationships that are needed during the acquirement
		TParameter sourceResourceParam = new TParameter();
		sourceResourceParam.setName(SOURCE_RESOURCE_PARAMETER_NAME);
		sourceResourceParam.setType((new QName(getTypeDefinitionsTargetNamespace(), SOURCE_RESOURCE_PARAMETER_NAME)).toString());

		// Dependency resource models
		TParameter targetResourceParam = new TParameter();
		targetResourceParam.setName(TARGET_RESOURCE_PARAMETER_NAME);
		targetResourceParam.setType((new QName(getTypeDefinitionsTargetNamespace(), TARGET_RESOURCE_PARAMETER_NAME)).toString());
		
		
		TOperation.InputParameters inputParameters = new TOperation.InputParameters();
		inputParameters.getInputParameter().add(sourceResourceParam);
		inputParameters.getInputParameter().add(targetResourceParam);
		operation.setInputParameters(inputParameters);
		
		return operation ;
	}


}
