package uni_stuttgart.ipsm.protocols.integration.operations;

import java.io.InputStream;
import java.util.List;
import org.oasis_open.docs.tosca.ns._2011._12.TOperation;


/**
 * An operation realization for instance a realization of the operation initialize for a resource
 * @author timur
 *
 */
public interface OperationRealization {
	
	
	
	
	/**
	 * Returns the list of resource types supported by this operation.
	 * In case a domain specific operation, this returns null and all are supported.
	 * @return
	 */
	List<String> getSupportedResources();
	
	
	/**
	 * Operation type
	 * @return
	 */
	OperationTypes getOperationType();
	
	
	/**
	 * Returns type definitions for this object. That's a schema file.
	 * @return
	 */
	InputStream getTypeDefinition();
	
	/**
	 * Returns targetNamespace of this operations. It should be a URI.
	 * @return
	 */
	String getTypeDefinitionsTargetNamespace();
	
	
	/**
	 * Operation definition
	 * @return
	 */
	TOperation getOperationDefinition();
	
	/**
	 * 
	 * @param resourceOrRelationship
	 * Target resource or relationship model
	 * @param callback
	 * Operation callback
	 * @param parameters
	 * Parameters expected
	 * @return
	 */
	void executeOperation(RunnableContainer resourceOrRelationshipContainer, Object inputParameters, OperationCallback callback);
	
	
}
