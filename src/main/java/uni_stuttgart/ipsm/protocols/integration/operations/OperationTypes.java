package uni_stuttgart.ipsm.protocols.integration.operations;

public enum OperationTypes {
	DOMAIN_SPECIFIC_OPERATION, NODE_SPECIFIC_OPERATION
}
