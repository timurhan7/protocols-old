package uni_stuttgart.ipsm.protocols.integration.operations;

import java.io.InputStream;

import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;




/**
 * Represents a deployable object
 * @author sungurtn
 *
 */
public interface RunnableContainer {
    /**
     * Get instance model of the respective resource or relationship
     * @return
     */
    TEntityTemplate getTargetModel();

    /**
     * Returns deployable represented by this deployable if available
     * @return
     */
    InputStream getDeployable();
}
