package uni_stuttgart.ipsm.protocols.services;

import javax.jws.WebService;
import javax.jws.WebParam;
import javax.jws.soap.SOAPBinding.*;
import javax.jws.soap.*;

/**
 * IPE Runtime service web interface
 *
 *
 */
@WebService(targetNamespace="http://iaas.uni-stuttgart.de/ipsm/execution")
@SOAPBinding(style=Style.DOCUMENT, use=Use.LITERAL, parameterStyle=ParameterStyle.WRAPPED)
public interface IPERuntimeService {
    /**
     * Converts an IPE model to an IPE instance
     * @return
     * If operation was successful
     */
    String createIPEInstance(@WebParam(partName = "ipeModel", name = "ipeModel") String ipeModel, 
    						 @WebParam(partName = "processId", name = "processId") String processId);
    /**
     * Generate an IPE model instance using the ipeModel and processId
     * @return
     * If operation was successful
     */
    boolean engageResources(@WebParam(partName = "ipeModelInstance", name = "ipeModelInstance") String ipeModelInstance);
    /**
     * Engages resources of an informal process as defined in the instance model
     * @return
     * result of the operation for error handling
     */
    boolean updateProcessInstance(@WebParam(partName = "oldInstanceModel", name = "oldInstanceModel") String oldInstanceModel, 
    							  @WebParam(partName = "newInstanceModel", name = "newInstanceModel") String newInstanceModel);
    /**
     * Store given instance model in a persistent way
     * @return
     * result of the operation for error handling
     */
    boolean storeIPEModelInstance(@WebParam(partName = "ipeModelInstance", name = "ipeModelInstance") String ipeModelInstance);
    /**
     * Converts instance model into a terminated process model with the given error
     * @return
     * result of the operation for error handling
     */
    String terminateInstanceWithError(@WebParam(partName = "ipeModelInstance", name = "ipeModelInstance") String ipeModelInstance, 
    								 @WebParam(partName = "errorDef", name = "errorDef") String errorDef);
    /**
     * Converts instance model into a terminated process model successfully
     * @return
     * terminated model definition
     */
    String terminateInstanceSuccessfully(@WebParam(partName = "ipeModelInstance", name = "ipeModelInstance") String ipeModelInstance);

}
